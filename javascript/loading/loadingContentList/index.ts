export const loadingContentList = (): void => {
	const content = document.getElementById('content')
	if (content) {
		const div = document.createElement('div')
		div.className = 'content-loading'
		div.setAttribute('id', 'content-loading')
		const text = document.createTextNode('Loading...')
		div.appendChild(text)
		content.appendChild(div)
	}
}
