export const loadingAsideList = (): void => {
	const aside = document.getElementById('aside')
	if (aside) {
		const list = document.createElement('ul')
		list.className = 'aside-list'
		const li = document.createElement('li')
		li.className = 'aside-list-li'
		li.setAttribute('id', 'aside-list-loading')
		const span = document.createElement('span')
		span.className = 'aside-list-li-a'
		const text = document.createTextNode('Loading...')
		span.appendChild(text)
		li.appendChild(span)
		list.appendChild(li)
		aside.appendChild(list)
	}
}
