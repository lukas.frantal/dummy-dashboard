import { initData } from './initData'

window.onload = (): void => {
	initData()
}
