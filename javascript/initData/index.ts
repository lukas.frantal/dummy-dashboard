import { initContent, initAside } from '../layout'
import { loadingAsideList, loadingContentList } from '../loading'

const data = {
	images: [
		{ name: '1', url: '' },
		{ name: '2', url: '' },
		{ name: '3', url: '' },
		{ name: '4', url: '' },
		{ name: '5', url: '' },
		{ name: '6', url: '' },
		{ name: '7', url: '' },
		{ name: '8', url: '' },
		{ name: '9', url: '' },
	],
	folders: [
		{
			id: '0',
			active: true,
			title: 'All Projects',
			href: '/',
			images: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
		},
		{
			id: '1',
			active: false,
			title: 'Folder 1',
			href: '/folder1',
			images: ['1', '2', '3', '4', '5', '6'],
		},
		{
			id: '2',
			active: false,
			title: 'Folder 2',
			href: '/folder2',
			images: ['1', '2', '3', '4'],
		},
		{
			id: '3',
			active: false,
			title: 'Folder 3',
			href: '/folder3',
			images: ['1', '2'],
		},
	],
}

export type dataType = typeof data
export type dataFolderType = typeof data.folders[0]
export type dataImagesType = typeof data.images[0]

const getData = (): Promise<dataType> =>
	new Promise(resolve => {
		setTimeout(() => {
			resolve(data)
		}, 2000)
	})

const initPage = (data: dataType): void => {
	initAside(data)
	initContent(data)
}

export const initData = (): void => {
	loadingAsideList()
	loadingContentList()

	getData()
		.then(res => {
			const oldData = window.localStorage.getItem('data')

			if (oldData === null) {
				window.localStorage.setItem('data', JSON.stringify(res))
				initPage(res)
			} else {
				initPage(JSON.parse(oldData))
			}
		})
		.catch(e => console.error(e))
}
