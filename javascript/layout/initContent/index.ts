import type { dataType, dataImagesType, dataFolderType } from '../../initData'

const filteredActiveFolder = (data: dataType): dataImagesType[] => {
	const activeFolderImages: dataImagesType[] = []
	data.folders.filter((folder): void => {
		if (folder.active) {
			data.images.map((image: dataImagesType): undefined | number => {
				if (folder.images.includes(image.name)) {
					return activeFolderImages.push(image)
				}
				return
			})
		}
	})
	return activeFolderImages
}

const handleDragStart = (li: HTMLLIElement): void => {
	li.addEventListener('dragstart', ({ dataTransfer }): void => {
		const countOfCheckedImages = document.querySelectorAll('[checked="true"]').length
		const ghostElement = document.createElement('div')
		const span = document.createElement('span')
		span.className = 'content-list-li-ghost-count'
		const number = document.createTextNode(String(countOfCheckedImages))
		span.appendChild(number)
		ghostElement.appendChild(span)

		ghostElement.classList.add('content-list-li-ghost')
		li.appendChild(ghostElement)
		dataTransfer && dataTransfer.setDragImage(ghostElement, 0, 0)
	})
}

const handleDraggingAside = (data: dataType): void => {
	const arrayOfFolderLink = [...document.querySelectorAll('.aside-list-li-a')]

	arrayOfFolderLink.map(link => {
		link.addEventListener('dragover', (e: Event): void => {
			e.preventDefault()
			link.classList.add('dragging')
		})
		link.addEventListener('dragleave', (): void => {
			link.classList.remove('dragging')
		})

		link.addEventListener('drop', (): void => {
			link.classList.remove('dragging')
			const activeFolder = link.getAttribute('name')
			const arrayOfCheckedImages = [...document.querySelectorAll('[checked="true"]')]
			const arrayOfCheckdImagesName = arrayOfCheckedImages.map(element => element.getAttribute('name'))
			if (arrayOfCheckdImagesName.length > 0) {
				const oldFoldersData = data.folders

				const newFoldersData = oldFoldersData.map((folder: dataFolderType): dataFolderType => {
					if (folder.id === activeFolder) {
						const oldImagesData = folder.images
						arrayOfCheckdImagesName.map((image: string | null): void => {
							if (image && !oldImagesData.includes(image)) {
								folder.images.push(image)
							}
						})
						folder.images
					}
					return folder
				})
				const newData = {
					images: data.images,
					folders: newFoldersData,
				}

				window.localStorage.setItem('data', JSON.stringify(newData))
				initContent(newData)

				arrayOfCheckedImages.map((element: Element): void => {
					element.removeAttribute('checked')
					element.setAttribute('draggable', 'false')
				})
			}
		})
	})
}

const handledeClickActiveCheckbox = (li: HTMLLIElement, data: dataType): void => {
	li.addEventListener('click', () => {
		if (li.getAttribute('checked') === null) {
			li.setAttribute('checked', 'true')
			li.setAttribute('draggable', 'true')

			handleDragStart(li)
			handleDraggingAside(data)
		} else {
			li.removeAttribute('checked')
			li.setAttribute('draggable', 'false')
		}
	})
}

export const initContent = (data: dataType): void => {
	const content = document.getElementById('content')
	if (content) {
		content.textContent = ''
		const list = document.createElement('ul')
		list.className = 'content-list'

		filteredActiveFolder(data).map(
			({
				name, // url
			}: dataImagesType): void => {
				const li = document.createElement('li')
				li.className = 'content-list-li'
				li.setAttribute('name', name)
				handledeClickActiveCheckbox(li, data)
				// TODO load url like image
				// console.log(url)

				const div = document.createElement('div')
				div.className = 'content-list-li-checkbox'
				li.appendChild(div)
				const span = document.createElement('span')
				span.className = 'content-list-li-text'
				const text = document.createTextNode(name)
				span.appendChild(text)
				li.appendChild(span)
				list.appendChild(li)
				content.appendChild(list)
			},
		)
	}
}
