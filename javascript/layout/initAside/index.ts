import type { dataType, dataFolderType } from '../../initData'
import { initContent } from '../initContent'

export const initAside = (data: dataType): void => {
	const aside = document.getElementById('aside')
	if (aside) {
		aside.textContent = ''
		const list = document.createElement('ul')
		list.className = 'aside-list'

		data?.folders.map(({ title, href, id, active }: dataFolderType): void => {
			const li = document.createElement('li')
			li.className = 'aside-list-li'
			const a = document.createElement('a')
			a.className = 'aside-list-li-a'
			a.setAttribute('href', href)
			a.setAttribute('name', id)
			const text = document.createTextNode(title)
			if (active) {
				a.classList.add('active')
			}
			a.addEventListener('click', e => {
				e.preventDefault()
				window.location.hash = href
				const isActiveFolders = document.querySelector('.active')

				isActiveFolders && isActiveFolders.classList.remove('active')
				a.classList.add('active')
				const oldFoldersData = data.folders
				const newFoldersData = oldFoldersData.map(item => {
					item.active = false
					if (item.id === id) {
						item.active = true
					}
					return item
				})

				const newData = {
					images: data.images,
					folders: newFoldersData,
				}
				window.localStorage.setItem('data', JSON.stringify(newData))
				initContent(newData)
			})
			li.appendChild(a).appendChild(text)
			list.appendChild(li)
		})
		aside.appendChild(list)
	}
}
