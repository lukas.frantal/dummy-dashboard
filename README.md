# Dummy-dashboard

## [Description task](https://www.notion.so/Dummy-Dashboard-1-3-e6b1a33f1747470293e3310a8d488e2c)

## [Demo](https://dummy-dashboard.lukasfrantal.com/)

## Solution

If you want improve something you can use development command:

```
yarn dev
```

If you want build the project and run it like static index.html you can use this command:

```
yarn build
```

- for development I use [parcel-bundler](https://parceljs.org/).
- for code style I used [prettier](https://prettier.io/)
- for statically analyzes my code to quickly find problems I used [eslint](https://eslint.org/)
- for better lint the project commit messages I used [commitlint](https://commitlint.js.org/#/) and [husky](https://typicode.github.io/husky/#/)
- task expected style preprocessing so I used [sass](https://sass-lang.com/)
- for better catch some issue during development I used [typescript](https://www.typescriptlang.org/)
- this project I deployed on the platform [Vercel](https://vercel.com/)
- if you want you can run the project inside [docker](https://hub.docker.com/repository/docker/frantallukas10/dummy-dashboard)
